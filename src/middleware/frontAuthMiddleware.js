const jwt = require('jsonwebtoken');

const frontAuthMiddleware = (req, res, next) => {
  const authorization = req.headers.cookie;

  if (!authorization) {
    return res.status(400).json({
      message: 'Please, provide authorization header',
    });
  }
  const cookieList = {};

  authorization.split(';').forEach(function(cookie) {
    let [name, ...rest] = cookie.split('=');
    name = name?.trim();
    if (!name) return;
    const value = rest.join('=').trim();
    if (!value) return;
    cookieList[name] = decodeURIComponent(value);
  });


  if (!cookieList.jwtToken) {
    return res.status(400).json({
      message: 'Please, include token to request',
    });
  }

  try {
    const tokenPayload = jwt.verify(cookieList.jwtToken, process.env.JWT_KEY);
    req.userProfile = tokenPayload;
    next();
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports = {
  frontAuthMiddleware,
};
