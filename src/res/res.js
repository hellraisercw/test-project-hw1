const wrongLoginPwd = `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="data:image/x-icon;base64,
  AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAg
  AAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAgMDAADd/wAAAAAAAAAAAAAAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIiI
  iIiIiIiIhERERERERIiIiIiIiIiIiIiIiIiIiIiIhERESIiIiIiIiIiIi
  IiIiIiIiIiIiIiIhERERESIiIiIiIiIiIiIiIiIiIiIiIiIhERERERERI
  iIiIiIiIiIiIiIiIiIiIiIAAAAAAAAAACICICIiAiAiIgIgIiICICIAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  AAAAAAAAA//8AACQkAAAkJAAA" rel="icon" type="image/x-icon" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="/index.css">
  <title>{{ title }}</title>
</head>
<nav class="blue darken-4">
  <div class="nav-wrapper">
    <div class="nav-elements">
      <a href="/" class="brand-logo">Notes</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <form action="/" method="POST">
    <h2>Login</h2>
    <div class="input-field">
      <input type="text" name="username" id="username"
      placeholder="Enter your login" required>
      <input type="password" name="password"
      placeholder="Enter your password" required>
      <span class="wrong">Wrong password!</span>
    </div>
    <button type="submit" class="btn">Login/Register</button>
  </form>
</div>
`;

const wrongChangePwd = `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="data:image/x-icon;base64,
  AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAg
  AAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAgMDAADd/wAAAAAAAAAAAAAAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIiI
  iIiIiIiIhERERERERIiIiIiIiIiIiIiIiIiIiIiIhERESIiIiIiIiIiIi
  IiIiIiIiIiIiIiIhERERESIiIiIiIiIiIiIiIiIiIiIiIiIhERERERERI
  iIiIiIiIiIiIiIiIiIiIiIAAAAAAAAAACICICIiAiAiIgIgIiICICIAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  AAAAAAAAA//8AACQkAAAkJAAA" rel="icon" type="image/x-icon" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="/index.css">
  <title>{{ title }}</title>
</head>
<nav class="blue darken-4">
  <div class="nav-wrapper">
    <div class="nav-elements">
      <a href="/" class="brand-logo">Notes</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="/notes">Notes</a></li>
        <li><a href="/create">Create</a></li>
        <li class="active"><a href="/settings">Settings</a></li>
        <li>
          <form action="/logout" method="POST">
            <label>
              <button type="submit" class="btn">Logout</button>
            </label>
          </form>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <form action="/changepwd" method="POST">
    <h5>Change password</h5>
    <div class="input-field">
      <input type="password" name="oldpassword"
      placeholder="Current password" required>
      <input type="password" name="newpassword"
      placeholder="New password" required>
      <span class="wrong">Wrong current password!</span>
    </div>
    <button type="submit" class="btn">Change password</button>
  </form>
  <h5>Delete account</h5>
  <form action="/delete" method="POST">
    <label class="delete">
      <input type="hidden" value="{{_id}}" name="id">
      <button class="btn btn-small btn-delete"
      type="submit">Delete account</button>
    </label>
  </form>
</div>
`;

const successfulChangePwd = `
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="data:image/x-icon;base64,
  AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAg
  AAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAgMDAADd/wAAAAAAAAAAAAAAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIiI
  iIiIiIiIhERERERERIiIiIiIiIiIiIiIiIiIiIiIhERESIiIiIiIiIiIi
  IiIiIiIiIiIiIiIhERERESIiIiIiIiIiIiIiIiIiIiIiIiIhERERERERI
  iIiIiIiIiIiIiIiIiIiIiIAAAAAAAAAACICICIiAiAiIgIgIiICICIAAA
  AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
  AAAAAAAAA//8AACQkAAAkJAAA" rel="icon" type="image/x-icon" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="/index.css">
  <title>{{ title }}</title>
</head>
<nav class="blue darken-4">
  <div class="nav-wrapper">
    <div class="nav-elements">
      <a href="/" class="brand-logo">Notes</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="/notes">Notes</a></li>
        <li><a href="/create">Create</a></li>
        <li class="active"><a href="/settings">Settings</a></li>
        <li>
          <form action="/logout" method="POST">
            <label>
              <button type="submit" class="btn">Logout</button>
            </label>
          </form>
        </li>
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  <form action="/changepwd" method="POST">
    <h5>Change password</h5>
    <div class="input-field">
      <input type="password" name="oldpassword"
      placeholder="Current password" required>
      <input type="password" name="newpassword"
      placeholder="New password" required>
      <span class="okay">Password changed succesfully!</span>
    </div>
    <button type="submit" class="btn">Change password</button>
  </form>
  <h5>Delete account</h5>
  <form action="/delete" method="POST">
    <label class="delete">
      <input type="hidden" value="{{_id}}" name="id">
      <button class="btn btn-small btn-delete"
      type="submit">Delete account</button>
    </label>
  </form>
</div>
`;

module.exports = {
  wrongLoginPwd,
  wrongChangePwd,
  successfulChangePwd,
};
