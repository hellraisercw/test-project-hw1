const {Note} = require('../models/Note');
const {User} = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const {wrongLoginPwd,
  wrongChangePwd,
  successfulChangePwd,
} = require('../res/res');

const renderUserNotesPage = async (req, res) => {
  const notes = await Note.find({userId: req.userProfile.userId}).lean();

  res.render('notes', {
    title: 'Notes list',
    isNotes: true,
    notes,
  });
};

const renderAddNotesPage = (req, res) => {
  res.render('create', {
    title: 'Create note',
    isCreate: true,
  });
};

const renderUserLoginPage = (req, res) => {
  res.render('index', {
    title: 'Login',
    isLogin: true,
  });
};

const renderUserSettingsPage = (req, res) => {
  res.render('settings', {
    title: 'Settings',
    isSettings: true,
  });
};

const renderToggleCompleteUserNote = async (req, res) => {
  const note = await Note.findById(req.body.id);

  note.completed = !!req.body.completed;
  await note.save();
  res.redirect('/notes');
};

const renderDeleteUserNote = async (req, res) => {
  await Note.findByIdAndDelete(req.body.id);
  res.redirect('/notes');
};

const renderAddUserNote = async (req, res) => {
  const note = new Note({
    text: req.body.text,
    userId: req.userProfile.userId,
  });

  await note.save();
  res.redirect('/notes');
};

const renderUserAuth = async (req, res, next) => {
  const {username, password} = req.body;
  const isUser = await User.findOne({username: username});

  if (!isUser) {
    const user = new User({
      username: username,
      password: await bcrypt.hash(password, 10),
    });
    const payload = {
      username: user.username,
      userId: user._id,
    };
    const jwtToken = jwt.sign(payload, process.env.JWT_KEY);

    await user.save().catch((err) => {
      next(err);
    });

    res.status(200).cookie('jwtToken', jwtToken, {
      httpOnly: true,
    }).redirect('/notes');
  } else if (await bcrypt.compare(String(password), String(isUser.password))) {
    const payload = {
      username: isUser.username,
      userId: isUser._id,
    };
    const jwtToken = jwt.sign(payload, process.env.JWT_KEY);

    res.status(200).cookie('jwtToken', jwtToken, {
      httpOnly: true,
    }).redirect('/notes');
  } else {
    res.status(400).format({
      'text/html': function() {
        res.send(wrongLoginPwd);
      },
    });
  }
};

const renderUserLogout = async (req, res, next) => {
  res.clearCookie('jwtToken')
      .redirect('/');
};

const renderChangeUserPassword = async (req, res) => {
  const user = await User.findById(req.userProfile.userId);
  const match = await bcrypt
      .compare(String(req.body.oldpassword), String(user.password));

  if (!match) {
    return res.status(400).format({
      'text/html': function() {
        res.send(wrongChangePwd);
      },
    });
  }
  try {
    user.password = await bcrypt.hash(req.body.newpassword, 10);
    await user.save();

    return res.status(200).format({
      'text/html': function() {
        res.send(successfulChangePwd);
      },
    });
  } catch (err) {
    throw err;
  }
};

const renderDeleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.userProfile.userId);
    await Note.deleteMany({userId: req.userProfile.userId});

    return res.clearCookie('jwtToken')
        .redirect('/');
  } catch (err) {
    throw err;
  }
};

module.exports = {
  renderUserNotesPage,
  renderAddNotesPage,
  renderUserLoginPage,
  renderUserSettingsPage,
  renderAddUserNote,
  renderToggleCompleteUserNote,
  renderDeleteUserNote,
  renderUserAuth,
  renderUserLogout,
  renderChangeUserPassword,
  renderDeleteUser,
};
