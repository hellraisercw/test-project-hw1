const bcrypt = require('bcryptjs');

const {User} = require('../models/User');

const getProfileInfo = async (req, res) => {
  try {
    const user = await User.findById(req.userProfile.userId);

    res.status(200).json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: new Date(req.userProfile.iat).toISOString(),
      },
    });
  } catch (err) {
    throw err;
  }
};

const changeProfilePassword = async (req, res) => {
  const user = await User.findById(req.userProfile.userId);
  const match = await bcrypt
      .compare(String(req.body.oldPassword), String(user.password));

  if (!match) {
    return res.status(400).json({
      message: 'Please enter correct current password',
    });
  }
  try {
    user.password = await bcrypt.hash(req.body.newPassword, 10);
    await user.save();

    return res.status(200).json({message: 'Success'});
  } catch (err) {
    throw err;
  }
};

const deleteProfile = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.userProfile.userId);

    res.status(200).json({message: 'Success'});
  } catch (err) {
    throw err;
  }
};

module.exports = {
  getProfileInfo,
  changeProfilePassword,
  deleteProfile,
};
