const express = require('express');
const {frontAuthMiddleware} = require('../middleware/frontAuthMiddleware');

const router = new express.Router();
const {
  renderUserNotesPage,
  renderAddNotesPage,
  renderUserLoginPage,
  renderUserSettingsPage,
  renderAddUserNote,
  renderToggleCompleteUserNote,
  renderDeleteUserNote,
  renderUserAuth,
  renderUserLogout,
  renderChangeUserPassword,
  renderDeleteUser,
} = require('../controllers/renderService');

router.get('/notes', frontAuthMiddleware, renderUserNotesPage);

router.get('/settings', frontAuthMiddleware, renderUserSettingsPage);

router.get('/create', frontAuthMiddleware, renderAddNotesPage);

router.get('/', renderUserLoginPage);

router.post('/complete', frontAuthMiddleware, renderToggleCompleteUserNote);

router.post('/create', frontAuthMiddleware, renderAddUserNote);

router.post('/delete', frontAuthMiddleware, renderDeleteUserNote);

router.post('/', renderUserAuth);

router.post('/logout', renderUserLogout);

router.post('/changepwd', frontAuthMiddleware, renderChangeUserPassword);

router.post('/deleteuser', frontAuthMiddleware, renderDeleteUser);


module.exports = {
  renderRouter: router,
};
