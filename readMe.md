Simple guide to run Notes app:

1. Pull the project.
2. Move to project folder in console.
3. Check "node -v" must be 16.16.0.
4. Run "npm i" command.
5. Run "npm start" command.
6. Open web browser at page "localhost:8080".
7. Enjoy the app!

Feedback is welcome. A lot of stuff needs to be improved.